<?php
namespace app\components;

use Yii;

trait getUserFileTrait {
    public function getFilePass()
    {
        return Yii::getAlias('@app') . DS . Yii::$app->params['upload_dir'] . DS . md5($this->username) . '.json';
    }
}