<?php

namespace app\controllers;

use app\models\Categories;
use app\models\Products;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;

class TestFourController extends Controller
{
    /**
     * action Index.
     *
     * @return string
     */
    public function actionIndex()
    {
        $products_query = Products::find();
        $products_count = clone $products_query;

        $pages = new Pagination(['totalCount' => $products_count->count()]);
        $products = $products_query
            ->joinWith('productsToCategories')
            ->joinWith('productsTags')
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $categories = Categories::find()
            ->select('cat.*,cat_list.parent_id')
            ->from('categories cat')
            ->join('LEFT JOIN', 'categories_list cat_list', 'cat_list.id = cat.id')
            ->where(['parent_id' => 0])
            ->orderBy('parent_id ASC')
            ->all();

        return $this->render('index', [
            'categories' => $categories,
            'products' => $products,
            'pages' => $pages,
        ]);
    }

    /**
     * action Categories
     *
     * @param $id
     * @return string
     */
    public function actionCategories($id){
        $cat=Categories::find()->where(['id'=>$id])->one();
        if (!empty($cat->childrenCategories)) {

            $child_cat_ids = new Categories();
            $child_cat_ids=$child_cat_ids->getCategoriesIds($cat->childrenCategories);
            if(is_array($child_cat_ids)){
                $child_cat_ids[]=$cat->id;
            }else{
                $child_cat_ids=[$child_cat_ids,$cat->id];
            }
        }else{
            $child_cat_ids=$cat->id;
        }

        $products_query = Products::find()
            ->from('products p')
            ->join('LEFT JOIN','products_to_categories ptc','p.id=ptc.products_id')
            ->where(['categories_id'=>$child_cat_ids])
            ->groupBy(['products_id']);
        $products_count = clone $products_query;

        $pages = new Pagination(['totalCount' => $products_count->count()]);
        $products = $products_query
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $categories = Categories::find()
            ->select('cat.*,cat_list.parent_id')
            ->from('categories cat')
            ->join('LEFT JOIN', 'categories_list cat_list', 'cat_list.id = cat.id')
            ->where(['parent_id' => 0])
            ->orderBy('parent_id ASC')
            ->all();

        return $this->render('index', [
            'categories' => $categories,
            'products' => $products,
            'pages' => $pages,
            'category'=>$cat
        ]);
    }

}
