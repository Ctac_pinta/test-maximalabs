<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class TestThreeController extends Controller
{
    /**
     * action Index.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

}
