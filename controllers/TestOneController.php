<?php

namespace app\controllers;

use app\models\AddUserFileForm;
use app\models\LoginUserFileForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class TestOneController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * action Index.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * action Add User
     *
     * @return string|Response
     */
    public function actionAddUser()
    {
        $model = new AddUserFileForm();
        if ($model->load(Yii::$app->request->post()) && $model->saveUserFile()) {
            return $this->redirect('/test-one');
        }
        return $this->render('add-user1', [
            'model' => $model
        ]);
    }

    /**
     * action Login
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user1->isGuest) {
            return $this->goBack('/test-one/success-login');
        }

        $model = new LoginUserFileForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack('/test-one/success-login');
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * action Success Login
     *
     * @return string
     */
    public function actionSuccessLogin()
    {
        if (Yii::$app->user1->isGuest) {
            return $this->goBack('/test-one/login');
        }
        return $this->render('success-login', [
            'username' => Yii::$app->user1->identity->username,
        ]);
    }

    /**
     * action Logout
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user1->logout();

        return $this->goHome();
    }
}
