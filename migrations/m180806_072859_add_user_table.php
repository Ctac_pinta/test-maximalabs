<?php

use yii\db\Migration;

/**
 * Class m180806_072859_add_user_table
 */
class m180806_072859_add_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull()->unique(),
            'password' => $this->text(),
            'authKey' => $this->string(255),
            'accessToken' => $this->string(255),
            'loginTryCount' => $this->integer()->defaultValue(0),
            'lastLogin' => $this->integer()->defaultValue(0)
        ]);

        $this->createIndex(
            'users_idx',
            'users',
            'id'
        );
        $this->createIndex(
            'users_username_idx',
            'users',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180806_072859_add_user_table cannot be reverted.\n";

        return false;
    }
    */
}
