<?php

use yii\db\Migration;

/**
 * Class m180803_095957_inser_data_to_products_to_tags
 */
class m180803_095957_inser_data_to_products_to_tags extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        for ($i = 1; $i <= 1000; $i++) {
            $this->insert('products_tags', [
                'products_id' => rand(1, 1000),
                'tags_id' => rand(1, 20),
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public
    function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE products_tags');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
        $this->execute('ALTER TABLE products_tags AUTO_INCREMENT=0');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_095957_inser_data_to_products_to_tags cannot be reverted.\n";

        return false;
    }
    */
}
