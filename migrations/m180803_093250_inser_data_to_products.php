<?php

use yii\db\Migration;

/**
 * Class m180803_093250_inser_data_to_products
 */
class m180803_093250_inser_data_to_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        for($i=1;$i<=1000;$i++){
            $this->insert('products',[
                'title'=>'Товар'.$i,
                'price'=>100,
                'count'=>rand(1,9999),
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE products');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
        $this->execute('ALTER TABLE products AUTO_INCREMENT=0');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_093250_inser_data_to_products cannot be reverted.\n";

        return false;
    }
    */
}
