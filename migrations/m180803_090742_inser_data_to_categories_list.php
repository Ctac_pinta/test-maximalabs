<?php

use yii\db\Migration;

use app\models\Categories;

/**
 * Class m180803_090742_inser_data_to_categories_list
 */
class m180803_090742_inser_data_to_categories_list extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $categories = Categories::find()->all();
        $level = 0;
        foreach ($categories as $cat) {
            if ($level != 4) {
                $this->insert('categories_list', [
                    'categories_id' => $cat->id,
                    'parent_id' => ($cat->id - 1),
                ]);
                $level++;
            } else {
                $this->insert('categories_list', [
                    'categories_id' => $cat->id,
                    'parent_id' => 0,
                ]);
                $level = 0;
            }

        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE categories_list');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
        $this->execute('ALTER TABLE categories_list AUTO_INCREMENT=0');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_090742_inser_data_to_categories_list cannot be reverted.\n";

        return false;
    }
    */
}
