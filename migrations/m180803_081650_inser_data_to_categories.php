<?php

use yii\db\Migration;

/**
 * Class m180803_081650_inser_data_to_categories
 */
class m180803_081650_inser_data_to_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        for($i=1;$i<=100;$i++){
            $this->insert('categories',[
                'title'=>'Катагория'.$i
            ]);
        }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE categories');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
        $this->execute('ALTER TABLE categories AUTO_INCREMENT=0');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_081650_inser_data cannot be reverted.\n";

        return false;
    }
    */
}
