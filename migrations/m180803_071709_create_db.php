<?php

use yii\db\Migration;

/**
 * Class m180803_071709_create_db
 */
class m180803_071709_create_db extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'product_count' => $this->integer()->defaultValue(0)
        ]);
        $this->createIndex(
            'categories_idx',
            'categories',
            'id'
        );
        $this->createIndex(
            'categories_title_idx',
            'categories',
            'title'
        );
        $this->createIndex(
            'categories_product_count_idx',
            'categories',
            'product_count'
        );

        $this->createTable('categories_list', [
            'id' => $this->primaryKey(),
            'categories_id' => $this->integer()->notNull(),
            'parent_id' => $this->integer()->defaultValue(0),
        ]);

        $this->createIndex(
            'categories_list_idx',
            'categories_list',
            'id'
        );
        $this->createIndex(
            'categories_list_categories_id_idx',
            'categories_list',
            'categories_id'
        );
        $this->createIndex(
            'categories_list_parent_id_idx',
            'categories_list',
            'parent_id'
        );

        $this->addForeignKey(
            'fk_categories_list_categories_id',
            'categories_list',
            'categories_id',
            'categories',
            'id',
            'CASCADE'
        );

        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'image' => $this->string(255),
            'title' => $this->string(255)->notNull(),
            'price' => $this->float(2)->notNull(),
            'count' => $this->integer()->defaultValue(0)->notNull(),
        ]);

        $this->createIndex(
            'products_idx',
            'products',
            'id'
        );

        $this->createIndex(
            'products_title_idx',
            'products',
            'title'
        );
        $this->createIndex(
            'products_price_idx',
            'products',
            'price'
        );
        $this->createIndex(
            'products_count_idx',
            'products',
            'count'
        );

        $this->createTable('products_to_categories', [
            'id' => $this->primaryKey(),
            'products_id' => $this->integer(),
            'categories_id' => $this->integer(),
        ]);

        $this->createIndex(
            'products_to_categories_idx',
            'products_to_categories',
            'products_id'
        );

        $this->createIndex(
            'products_to_categories_categories_idx',
            'products_to_categories',
            'categories_id'
        );

        $this->addForeignKey(
            'fk_products_to_categories_categories_id',
            'products_to_categories',
            'categories_id',
            'categories',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_products_to_categories_products_id',
            'products_to_categories',
            'products_id',
            'products',
            'id',
            'CASCADE'
        );

        $this->createTable('tags', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
        ]);

        $this->createIndex(
            'tags_idx',
            'tags',
            'id'
        );

        $this->createTable('products_tags', [
            'id' => $this->primaryKey(),
            'products_id' => $this->integer(),
            'tags_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk_products_tags_products_id',
            'products_tags',
            'products_id',
            'products',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_products_tags_tags_id',
            'products_tags',
            'tags_id',
            'tags',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_categories_list_categories_id',
            'categories_list'
        );
        $this->dropForeignKey(
            'fk_products_to_categories_categories_id',
            'products_to_categories'
        );
        $this->dropForeignKey(
            'fk_products_to_categories_products_id',
            'products_to_categories'
        );
        $this->dropForeignKey(
            'fk_products_tags_products_id',
            'products_tags'
        );
        $this->dropForeignKey(
            'fk_products_tags_tags_id',
            'products_tags'
        );
        $this->dropTable('tags');
        $this->dropTable('products_tags');
        $this->dropTable('categories_list');
        $this->dropTable('products');
        $this->dropTable('categories');
        $this->dropTable('products_to_categories');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_071709_create_db cannot be reverted.\n";

        return false;
    }
    */
}
