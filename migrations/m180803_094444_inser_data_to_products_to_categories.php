<?php

use yii\db\Migration;

/**
 * Class m180803_094444_inser_data_to_products_to_categories
 */
class m180803_094444_inser_data_to_products_to_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        for ($i = 1; $i <= 1000; $i++) {
                $this->insert('products_to_categories', [
                    'products_id' => rand(1, 1000),
                    'categories_id' => rand(1, 99),
                ]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE products_to_categories');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
        $this->execute('ALTER TABLE products_to_categories AUTO_INCREMENT=0');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_094444_inser_data_to_products_to_categories cannot be reverted.\n";

        return false;
    }
    */
}
