<?php

use yii\db\Migration;


/**
 * Class m180803_154143_add_product_count
 */
class m180803_154143_add_product_count extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $categories = \app\models\Categories::find()
            ->select('cat.*,cat_list.parent_id')
            ->from('categories cat')
            ->join('LEFT JOIN', 'categories_list cat_list', 'cat_list.id = cat.id')
            ->all();
        foreach ($categories as $v) {
            if (!empty($v->childrenCategories)) {

                $child_cat_ids = new \app\models\Categories();
                $child_cat_ids=$child_cat_ids->getCategoriesIds($v->childrenCategories);
                if(is_array($child_cat_ids)){
                    $child_cat_ids[]=$v->id;
                }else{
                    $child_cat_ids=[$child_cat_ids,$v->id];
                }
                $v->product_count=\app\models\ProductsToCategories::find()->where(['categories_id'=>$child_cat_ids])->groupBy(['products_id'])->count();
                $v->save();
            } else {
                $v->product_count=\app\models\ProductsToCategories::find()->where(['categories_id'=>$v->id])->groupBy(['products_id'])->count();
                $v->save();
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \app\models\Categories::updateAll(['product_count' => 0], ['>', 'product_count', 0]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_154143_add_product_count cannot be reverted.\n";

        return false;
    }
    */
}
