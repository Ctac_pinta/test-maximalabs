<?php

use yii\db\Migration;

/**
 * Class m180803_093740_inser_data_to_tags
 */
class m180803_093740_inser_data_to_tags extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        for ($i = 1; $i <= 20; $i++) {
            $this->insert('tags', [
                'title' => 'Тег' . $i,
            ]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE tags');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
        $this->execute('ALTER TABLE tags AUTO_INCREMENT=0');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_093740_inser_data_to_tags cannot be reverted.\n";

        return false;
    }
    */
}
