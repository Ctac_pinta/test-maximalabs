<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * AddUserForm is the model behind the add add user to file form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginUserForm extends Model
{

    public $username;
    public $password;
    public $rememberMe = true;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            [['username'], 'validateUsername'],
            [['password'], 'validatePassword'],
        ];
    }

    /**
     * Validates the username.
     * This method serves as the inline validation for username
     *
     * @param $attribute
     * @param $params
     */
    public function validateUsername($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (empty($user)) {
                $this->addError($attribute, 'Пользователь с таким именем не найден.');
            }
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    /**
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ($user->loginTryCount >= 3 && $user->lastLogin > time()) {
                Yii::$app->getSession()->setFlash('error', 'Попробуйте еще раз через:' . ($user->lastLogin - time()) . ' сек');
                $this->addError($attribute);
                return false;
            }
            if (empty($user) || !Yii::$app->getSecurity()->validatePassword($this->password, $user->password)) {
                $user->loginTryCount = $user->loginTryCount + 1;
                $user->lastLogin = time() + 5 * 60;
                if (!$user->save()) {
                    Yii::$app->getSession()->setFlash('error', 'Возникла проблема при авторизации');
                }
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $login_user = new User($user);
            $login = Yii::$app->user->login($login_user, $this->rememberMe ? 3600 * 24 * 30 : 0);
            if ($login) {
                $user->loginTryCount = 0;
                if (! $user->save()) {
                    Yii::$app->getSession()->setFlash('error', 'Возникла проблема при авторизации');
                }
            }
            return $login;
        }
    }

    public
    function register()
    {

    }

    public
    function getUser()
    {
        $user = Users::find()->where(['username' => $this->username])->one();
        if (!empty($user)) {
            return $user;
        }
        return false;
    }
}
