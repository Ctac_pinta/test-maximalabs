<?php

namespace app\models;

use app\components\getUserFileTrait;
use Yii;
use yii\base\Model;

/**
 * AddUserForm is the model behind the add add user to file form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginUserFileForm extends Model
{
    use getUserFileTrait;

    public $username;
    public $password;
    public $rememberMe = true;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            [['username'], 'validateUsername'],
            [['password'], 'validatePassword'],
        ];
    }

    /**
     * Validates the username.
     * This method serves as the inline validation for username
     *
     * @param $attribute
     * @param $params
     */
    public function validateUsername($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!file_exists($this->getFilePass())) {
                $this->addError($attribute, 'Пользователь с таким именем не найден.');
            }
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    /**
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!file_exists($file = $this->getFilePass())) {
                $this->addError($attribute, 'Incorrect username or password1.');
            }
            $user = $this->getUser($file);
            if($user->loginTryCount>=3&&$user->lastLogin>time()){
                Yii::$app->getSession()->setFlash('error', 'Попробуйте еще раз через:'. ($user->lastLogin-time()).' сек');
                $this->addError($attribute);
                return false;
            }
            if (empty($user) || !Yii::$app->getSecurity()->validatePassword($this->password, $user->password) ) {
                $user->loginTryCount = $user->loginTryCount+1;
                $user->lastLogin = time()+5*60;
                if (!file_put_contents($this->getFilePass(), json_encode($user))) {
                    Yii::$app->getSession()->setFlash('error', 'Возникла проблема при авторизации');
                }
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        $file = $this->getFilePass();
        if ($this->validate()) {
            if (file_exists($file)) {
                $user = $this->getUser($file);
                $login = Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
                if ($login){
                    $user->loginTryCount = 0;
                    if (!file_put_contents($this->getFilePass(), json_encode($user))) {
                        Yii::$app->getSession()->setFlash('error', 'Возникла проблема при авторизации');
                    }
                }
                return $login;
            }
        }

        return false;
    }

    public function getUser($file)
    {
        $user = file_get_contents($file);
        if ($user = json_decode($user)) {
            $model = new UserFile($user);
            return $model;
        }
        return false;
    }
}
