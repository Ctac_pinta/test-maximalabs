<?php

namespace app\models;

use app\components\getUserFileTrait;
use Yii;
use yii\base\Model;

/**
 * AddUserForm is the model behind the add add user to file form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class AddUserFileForm extends Model
{
    use getUserFileTrait;

    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            [['username'], 'validateUsername'],
        ];
    }

    /**
     * Validates the username.
     * This method serves as the inline validation for username
     *
     * @param $attribute
     * @param $params
     */
    public function validateUsername($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (file_exists($this->getFilePass())) {
                $this->addError($attribute, 'Пользователь с таким именем уже существует.');
            }
        }
    }


    public function saveUserFile()
    {
        if ($this->validate()) {
            $user =  [
                'id' => md5($this->username),
                'username' => $this->username,
                'password' => Yii::$app->getSecurity()->generatePasswordHash($this->password),
                'authKey' => md5($this->username),
                'accessToken' => md5($this->username),
                'loginTryCount' => 0,
                'lastLogin' => 0,
            ];
            if ($user = json_encode($user)) {
                if (!file_put_contents($this->getFilePass(), $user)) {
                    Yii::$app->getSession()->setFlash('error', 'Пользователь не создан');
                    return false;
                }
            }
            Yii::$app->getSession()->setFlash('success', 'Пользователь создан');
            return true;
        }
        return false;
    }
}
