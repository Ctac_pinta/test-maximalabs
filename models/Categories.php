<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $title
 *
 * @property CategoriesList[] $
 *
 * @property ProductsToCategories[] $productsToCategories
 * @property ProductsToCategories[] $childrenCategories
 */
class Categories extends \yii\db\ActiveRecord
{
    public $breadcrumbs = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesLists()
    {
        return $this->hasOne(CategoriesList::className(), ['categories_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildrenCategories()
    {
        return $this->hasMany(CategoriesList::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsToCategories()
    {
        return $this->hasMany(ProductsToCategories::className(), ['categories_id' => 'id']);
    }

    /**
     * @param $categories
     * @param int $k
     * @return array
     */
    public function getCategoriesIds($categories)
    {
        $answer = [];
        foreach ($categories as $v) {
            if (!empty($v->categories->childrenCategories)) {
                $res = $this->getCategoriesIds($v->categories->childrenCategories, 1);
                if (is_array($res)) {
                    $answer = array_merge($answer, $res);
                } else {
                    array_push($answer, $res);
                }
                array_push($answer, $v->categories_id);
                return $answer;
            } else {
                return $v->categories_id;
            }
        }
        return $answer;
    }

    public function breadcrumbBuild($cat){

        if ($cat->parent_id != 0) {
            $this->breadcrumbs[] = [
                'label' => $cat->parentCategories->title,
                'url' => '/test-two/categories?id='.$cat->parent_id
            ];
            $this->breadcrumbBuild($cat->parentCategories->categoriesLists);
        }

        return $this->breadcrumbs;
    }
}
