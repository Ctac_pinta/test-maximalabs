<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories_list".
 *
 * @property int $id
 * @property int $categories_id
 * @property int $parent_id
 *
 * @property Categories $categories
 */
class CategoriesList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categories_id'], 'required'],
            [['categories_id', 'parent_id'], 'integer'],
            [['categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['categories_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categories_id' => 'Categories ID',
            'parent_id' => 'Parent ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasOne(Categories::className(), ['id' => 'categories_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategories()
    {
        return $this->hasOne(Categories::className(), ['id' => 'parent_id']);
    }
}
