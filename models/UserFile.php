<?php

namespace app\models;

use Yii;

class UserFile extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $loginTryCount;
    public $lastLogin;

    /**
     * @param array $user
     */
    public function __construct($user)
    {
        $this->id = $user->id;
        $this->username = $user->username;
        $this->password = $user->password;
        $this->authKey = $user->authKey;
        $this->accessToken = $user->accessToken;
        $this->loginTryCount = $user->loginTryCount;
        $this->lastLogin = $user->lastLogin;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        $user_file= Yii::getAlias('@app') . DS . Yii::$app->params['upload_dir'] . DS . $id . '.json';
        if(file_exists($user_file)){
            $user = file_get_contents($user_file);
            if ($user = json_decode($user)) {
                return new static($user);
            }
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        var_dump("token");die();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
