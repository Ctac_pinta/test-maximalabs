<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = ['label' => 'Задание 1', 'url' => ['/test-one']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-center">
    <div class="h1">Добрый день, <span class="danger"><?=$username?></span></div>
    <div class="text-center"><a href="/test-one/logout" class="btn btn-success">Выход</a></div>
</div>
