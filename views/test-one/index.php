<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\widgets\ListView;
/* @var $this yii\web\View */

$this->title = 'Задание 1';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-container">
    <div class="h3">
        <a href="/test-one/add-user">Добавить пользователя</a>
    </div>
    <div class="h3">
        <a href="/test-one/login">Авторизация</a>
    </div>
</div>
