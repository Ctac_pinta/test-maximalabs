<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\widgets\ListView;
/* @var $this yii\web\View */

$this->title = 'Тестовое задание';

?>
<div class="site-index">
    <div class="bs-callout">
        <div class="h3">
            <a href="/test-one">Задание 1</a>
        </div>
        <div>
            Создайте пример системы аутентификации пользователей. Система должна предоставлять форму входа (имя
            пользователя и пароль) и страницу пользователя в системе.
        </div>
    </div>
    <div class="bs-callout">
        <div class="h3">
            <a href="/test-two">Задание 2</a>
        </div>
        <div>
            Товары на сайт интернет-магазина сгруппированы по категориям. Категории организованы в древовидную
            структуру с уровнем вложенности до 4 включительно. Значимые атрибуты категории: название. Значимые
            атрибуты товара: название, цена, количество на складе. Один продукт может относиться к нескольким
            категориям. Помимо категорий, товар также характеризуется набором тегов (максимум 20 тегов на все
            товары).
        </div>
    </div>
    <div class="bs-callout">
        <div class="h3">
            <a href="/test-three">Задание 3</a>
        </div>
        <div>
            Напишите javascript функцию преобразования числа в его текстовое представление (26 => “двадцать шесть”),
            входящее число находится в диапазоне 1..9999.
        </div>
    </div>

    <div class="bs-callout">
        <div class="h3">
            <a href="/test-four">Задание 4</a>
        </div>
        <div>
            Подключите базу данных из задания 2 к системе аутентификации из задания 1. Создайте в этой системе
            страницу, которая выводит на экран список товаров. Разместите на этой странице дополнительные элементы
            управления, которые вы считаете необходимыми.
        </div>
    </div>
</div>
