<div class="container">
    <form onsubmit="PrintText(); return false;">
        <div class="text-center form-group">
            <input id="number-field" class="form-control" type="number" min="1" max="9999" pattern="\d*" value="" required/>
        </div>
        <div class="text-center form-group">
            <input type="submit" class="btn btn-success" value="Печать"/>
        </div>

        <div id="print-text" class="text-center form-group">

        </div>
    </form>
</div>