<?php
/* @var $categories app\models\Categories */
/* @var $cat app\models\Categories */
?>
<div class="just-padding">
    <div class="cat-list list-group list-group-root well">
        <? foreach ($categories as $k => $cat_level1) { ?>
            <div class="list-group-item">
                <? if (!empty($cat_level1->childrenCategories)) { ?>
                    <a href="#item-<?= $cat_level1->id ?>" class="arrow" data-toggle="collapse">
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </a>
                <? } ?>
                <a href="/test-two/categories?id=<?=$cat_level1->id?>" class=""><?= $cat_level1->title ?> (<?= $cat_level1->product_count ?>)</a>
            </div>
            <? if (!empty($cat_level1->childrenCategories)) { ?>
                <div class="list-group collapse" id="item-<?= $cat_level1->id ?>">
                    <? foreach ($cat_level1->childrenCategories as $cat_level2) { ?>
                        <div class="list-group-item">
                            <? if (!empty($cat_level2->categories->childrenCategories)) { ?>
                                <a href="#item-<?= $cat_level1->id ?>-<?= $cat_level2->categories->id ?>"
                                   class="arrow" data-toggle="collapse">
                                    <i class="glyphicon glyphicon-chevron-right"></i>
                                </a>
                            <? } ?>
                            <a href="/test-two/categories?id=<?= $cat_level2->categories->id ?>" class="">
                                <?= $cat_level2->categories->title ?>
                                (<?= $cat_level2->categories->product_count ?>)
                            </a>
                        </div>
                        <? if (!empty($cat_level2->categories->childrenCategories)) { ?>
                            <div class="list-group collapse"
                                 id="item-<?= $cat_level1->id ?>-<?= $cat_level2->categories->id ?>">
                                <? foreach ($cat_level2->categories->childrenCategories as $cat_level3) { ?>
                                    <div class="list-group-item">
                                        <? if (!empty($cat_level3->categories->childrenCategories)) { ?>
                                            <a href="#item-<?= $cat_level1->id ?>-<?= $cat_level2->categories->id ?>-<?= $cat_level3->categories->id ?>"
                                               class="arrow" data-toggle="collapse">
                                                <i class="glyphicon glyphicon-chevron-right"></i>
                                            </a>
                                        <? } ?>
                                        <a href="/test-two/categories?id=<?=$cat_level3->categories->id ?>" class="">
                                            <?= $cat_level3->categories->title ?>
                                            (<?= $cat_level3->categories->product_count ?>)
                                        </a>
                                    </div>
                                    <? if (!empty($cat_level3->categories->childrenCategories)) { ?>
                                        <div class="list-group collapse"
                                             id="item-<?= $cat_level1->id ?>-<?= $cat_level2->categories->id ?>-<?= $cat_level3->categories->id ?>">
                                            <? foreach ($cat_level3->categories->childrenCategories as $cat_level4) { ?>
                                                <div class="list-group-item">
                                                    <a href="/test-two/categories?id=<?= $cat_level4->categories->id ?>"
                                                       class="" data-toggle="collapse">
                                                        <?= $cat_level4->categories->title ?>
                                                        (<?= $cat_level4->categories->product_count ?>)
                                                    </a>
                                                </div>
                                            <? } ?>
                                        </div>
                                    <? } ?>
                                <? } ?>

                            </div>
                        <? } ?>
                    <? } ?>
                </div>
            <? } ?>
        <? } ?>

    </div>
</div>