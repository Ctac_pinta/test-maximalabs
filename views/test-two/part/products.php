<?php
/* @var $products app\models\Products */
/* @var $v app\models\Products */
/* @var $pc app\models\productsToCategories */
?>
<div id="products" class="row list-group">
    <? foreach ($products as $k => $v) { ?>
        <div class="item  col-xs-4 col-lg-4">
            <div class="thumbnail">
                <? $img = !empty($v->image) ? $v->image : Yii::$app->params['def_img'] ?>
                <img class="group list-group-image" src="<?= $img ?>" alt=""/>

                <div class="caption">
                    <h4 class="group inner list-group-item-heading">
                        <?= $v->title ?>
                    </h4>

                    <div class="group inner list-group-item-text">
                        <?$arr_count=count($v->productsToCategories);
                        foreach ($v->productsToCategories as $k => $pc){ ?>
                            <a href="<?=$pc->categories_id?>"><?=$pc->categories->title?></a>
                            <?= ($arr_count-1)!=$k?', ':'';?>
                        <? } ?>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <p class="lead">
                                <?= $v->price ?>$
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <p class="text-center">
                                На складе:<br>
                                <?= $v->count ?> шт.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>
</div>