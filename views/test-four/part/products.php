<?php
/* @var $products app\models\Products */
/* @var $v app\models\Products */
/* @var $pc app\models\productsToCategories */

function PrintText($number) {
     $str = '';
     $number = strval(intval($number));
     $numbers4 = [
        ['одина', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
        ['десять', 'двадцать ', 'тридцать ', 'сорок ', 'пятьдесят ', 'шестьдесят ', 'семьдесят ', 'восемьдесят ', 'девяносто '],
        ['сто ', 'двести ', 'триста ', 'четыреста ', 'пятьсот ', 'шестьсот ', 'семьсот ', 'восемьсот ', 'девятьсот '],
        ['одна тысяча ', 'две тысячи ', 'три тысячи ', 'четыре тысячи ', 'пять тысячь ', 'шесть тысячь ', 'семь тысячь ', 'восемь тысячь ', 'девять тысячь '],
    ];
    if ($number > 0 && $number <= 9999) {
        $length=strlen($number);
        for ($i = 0; $i < $length ; $i++) {
            if (($number[$i] - 1) >= 0) {
                $x = $number[$i] . $number[$i + 1];
                if ($x > 10 && $x < 20 && $length == 4 && $i > 1 || $x > 10 && $x < 20 && $length == 3 && $i > 0 || $x > 10 && $x < 20 && $length == 2) {
                    $str .= $numbers4[0][$number[$i + 1] - 1] . 'надцать';
                    return $str;
                }

                $str .= $numbers4[$length - 1 - $i][$number[$i] - 1];
            }
        }
        return $str;
    }
    return false;
}

?>
<div id="products" class="row list-group">
    <? foreach ($products as $k => $v) { ?>
        <div class="item  col-xs-4 col-lg-4">
            <div class="thumbnail">
                <? $img = !empty($v->image) ? $v->image : Yii::$app->params['def_img'] ?>
                <img class="group list-group-image" src="<?= $img ?>" alt=""/>

                <div class="caption">
                    <h4 class="group inner list-group-item-heading">
                        <?= $v->title ?>
                    </h4>

                    <div class="group inner list-group-item-text">
                        <?$arr_count=count($v->productsToCategories);
                        foreach ($v->productsToCategories as $k => $pc){ ?>
                            <a href="<?=$pc->categories_id?>"><?=$pc->categories->title?></a>
                            <?= ($arr_count-1)!=$k?', ':'';?>
                        <? } ?>
                    </div>

                    <div class="group inner list-group-item-text">
                        <?$arr_count=count($v->productsTags);
                        foreach ($v->productsTags as $k => $pt){ ?>
                            <a href="javascript:;"><?=$pt->tags->title?></a>
                            <?= ($arr_count-1)!=$k?', ':'';?>
                        <? } ?>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <p class="lead">
                                <?= $v->price ?>$
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <p class="text-center">
                                На складе:<br>
                                <?if ($v->count!=0) {
                                    echo PrintText($v->count) . ' шт';
                                }else{
                                    echo 'Нет на складе';
                                }
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>
</div>