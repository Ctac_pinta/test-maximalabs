<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $products app\models\Products */
/* @var $v app\models\Products */
/* @var $pages \yii\data\Pagination */
/* @var $categories app\models\Categories */
/* @var $category app\models\Categories */

$this->title = 'Задание 4';

$this->params['breadcrumbs'][] = ['label'=>$this->title,'url'=>'/test-four/'];
if(!empty($category)){
    $bredcrumb=$category->breadcrumbBuild($category->categoriesLists);
    $this->params['breadcrumbs']=array_merge($this->params['breadcrumbs'],array_reverse ($bredcrumb));
}
$this->params['breadcrumbs'][] = $category->title;

?>
<div class="container">
    <div class="well well-sm">
        <strong>Category Title</strong>

        <div class="btn-group">
            <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
            </span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
                    class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-lg-4">
            <?= $this->render('part/category_list', [
                'categories' => $categories
            ]); ?>
        </div>
        <div class="col-md-8 col-lg-8">
            <?= $this->render('part/products', [
                'products' => $products
            ]); ?>
        </div>
    </div>

    <div class="row text-center">
        <?= \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
        ]); ?>
    </div>
</div>